"use strict";
var DEBUG = false;

function debug(str, group = false) {
    if (DEBUG) {
        if (group) { console.group(); }
        console.log(str);
        if (group) { console.groupEnd(); }
    }
}

function init(d) {
    DEBUG = d;
    return debug;
}

module.exports.init = init;