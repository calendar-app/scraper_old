var cron = require('node-cron');

console.log('Cron started!');
console.log('Execute at 12:00 on Friday, Saturday, and Sunday');
cron.schedule('*/15 6-23 * * *', () => {
    new require('./main').main();
});