"use strict";
const cfg = require(__dirname + '/config');

const scraperWEB = require('./scraper-web');
const scraperCSV = require('./scraper-csv');
const _ = require('underscore');
const strmatch = require('string-similarity');
const moment = require('moment');
moment.locale('it');

const debug = require('./debug').init(cfg.DEBUG);

var mydb, db;
var courseTable;
var today

debug('Starting...');

/**
 * Compare csv and web data received and return a single data array 
 * @param {Array} csv Contain all data received from google sheet
 * @param {Array} web COntain all data received from web site
 */
function merge(csv, web) {
    var results = [];

    csv.forEach(csvItem => {
        var match = _.find(web, webItem => {
            return moment(csvItem.date).isSame(moment(webItem.date))
                && csvItem.time.start === webItem.time.start
                && csvItem.time.end === webItem.time.end
                && (strmatch.compareTwoStrings(csvItem.prof, webItem.prof) * 100) > 70;
        });

        results.push({
            webID: match ? match.id : null, //web
            courseName: csvItem.course,
            moduleName: csvItem.title,
            required: csvItem.required,
            prof: csvItem.prof,
            place: {
                building: match ? match.place.building : null,  //web
                room: match ? match.place.room : null           //web
            },
            date: csvItem.date,
            time: { start: csvItem.time.start, end: csvItem.time.end },
            note: csvItem.note ? csvItem.note : null
        });

        /*debug({
            //course: { s1: csvItem.course, s2: match.course, percentage: Math.floor(strmatch.compareTwoStrings(csvItem.course, match.course) * 100) + '%' },
            title: { s1: csvItem.title, s2: match.title, percentage: Math.floor(strmatch.compareTwoStrings(csvItem.title, match.title) * 100) + '%' },
            prof: { s1: csvItem.prof, s2: match.prof, percentage: Math.floor(strmatch.compareTwoStrings(csvItem.prof, match.prof) * 100) + '%' }
        }, true);*/
    });

    return results;
}

async function uploadUncompletedRecords(data, csvCode) {

    //first of all select only times related to the current downloading process the haven't attribute webID
    var table = await db.query(`SELECT Times.id as id, Times.moduleName as moduleName, Times.required as required,  
                                    Professor.name as prof, Times.date as date, Times.timestart as timestart, Times.timeend as timeend,
                                    Times.note as note
                                FROM Times
                                    JOIN Course ON Times.id_course = Course.id
                                    JOIN Professor ON Times.id_professor = Professor.id
                                WHERE Course.csvCode = ? AND (webID IS NULL OR Times.id_room IS NULL)`, [csvCode]);

    if (table.length) {
        debug(table.length + ' uncompleted record to update', true);

        //update record's missing fields

        //iterate every uncompleted item received from db
        for (var i = 0; i < table.length; i++) {
            var dbItem = table[i];

            //check if every attribute correspond with one of downloaded data
            for (var j = 0; j < data.length; j++) {
                var item = data[j];

                /*console.log(`${dbItem.moduleName}:${item.moduleName}
                ${dbItem.required}:${item.required ? 1 : 0}
                ${dbItem.prof}:${item.prof}
                ${moment(dbItem.date).format('YYYY-MM-DD')}:${moment(item.date).format('YYYY-MM-DD')}
                ${parseInt(dbItem.timestart.split(':')[0])}:${parseInt(item.time.start.split(':')[0])}
                ${parseInt(dbItem.timestart.split(':')[1])}:${parseInt(item.time.start.split(':')[1])}
                ${parseInt(dbItem.timeend.split(':')[0])}:${parseInt(item.time.end.split(':')[0])}
                ${parseInt(dbItem.timeend.split(':')[1])}:${parseInt(item.time.end.split(':')[1])}
                ${dbItem.note}:${item.note}\n`);*/


                if (moment(dbItem.date).format('YYYY-MM-DD') === (moment(item.date).format('YYYY-MM-DD'))
                    && parseInt(dbItem.timestart.split(':')[0]) === parseInt(item.time.start.split(':')[0])
                    && parseInt(dbItem.timestart.split(':')[1]) === parseInt(item.time.start.split(':')[1])
                    && parseInt(dbItem.timeend.split(':')[0]) === parseInt(item.time.end.split(':')[0])
                    && parseInt(dbItem.timeend.split(':')[1]) === parseInt(item.time.end.split(':')[1])
                    /* && dbItem.note === item.note*/) {

                    //update webID and room if they exist (if they are updated from last downloading)
                    if (item.webID) {
                        var res = await db.query(`UPDATE Times SET webID = ?, id_room = (SELECT id FROM Room WHERE name = ?) WHERE id = ?`, [item.webID, (item.place.room ? item.place.room.trim() : item.place.room), dbItem.id]);  //jshint ignore: line
                        debug(`update uncompleted record - webID: ${item.webID}  changedRows: ${res.changedRows}`, true);
                        await db.query(`INSERT INTO log (action, courseCode, prof, times) VALUES (?, ?, 0, 1)`, ['update', csvCode]);

                        //remove current item to the data array to prevent updating it as duplicate in database
                        //(because comparing function use webID, but item in database doesn't contain webDB)
                        data = data.filter(dt => { return JSON.stringify(dt) !== JSON.stringify(item); }); // jshint ignore: line
                    }
                    break;
                }
            }
        }

        return data;
    } else {
        debug('No uncompleted record to update', true);
        return data;
    }
}

/**
 * filter data by date, return only data where date is the same or after today
 * excludes items which datetimes start are before the current datetime
 * @param {Array} data array of scraped data
 */
function filterByDate(data) {
    return _.filter(data, item => {
        return moment(moment(item.date).format('DD/MM/YYYY') + ' ' + item.time.start, 'DD/MM/YYYY H:mm')
            .isSameOrAfter(today);
    });
}

/**
 * return data which records aren't stored into db 
 * @param {Array} data array of scraped data
 */
async function filterByExistingRecords(scrapedData, csvCode) {
    var table = await db.query(`SELECT Times.* 
                                FROM Times JOIN Course ON Times.id_course = Course.id
                                WHERE Course.csvCode = ?`, [csvCode]);
    var results = [];
    scrapedData.forEach(scrapedItem => {
        var alreadyExist = _.find(table, dbItem => {
            return scrapedItem.moduleName === dbItem.moduleName
                && moment(scrapedItem.date).format('DD/MM/YYYY') === moment(dbItem.date).format('DD/MM/YYYY')
                && parseInt(scrapedItem.time.start.split(':')[0]) === parseInt(dbItem.timestart.split(':')[0])
                && parseInt(scrapedItem.time.start.split(':')[1]) === parseInt(dbItem.timestart.split(':')[1])
                && parseInt(scrapedItem.time.end.split(':')[0]) === parseInt(dbItem.timeend.split(':')[0])
                && parseInt(scrapedItem.time.end.split(':')[1]) === parseInt(dbItem.timeend.split(':')[1])
        });
        if (!alreadyExist) results.push(scrapedItem);
    });
    return results;
}

async function updateDBprof(item) {
    if (item.prof) {
        var dbProfs = await db.query('SELECT * FROM Professor');

        if (!dbProfs.length) { throw new Error(`The query: 'SELECT * FROM Professor' return 0 data`); }

        dbProfs = dbProfs.map(item => { return item.name; });

        // find best match beetween item.prof (audited string) and dbProfs (list with names to compare)
        var matchResult = strmatch.findBestMatch(item.prof, dbProfs);

        // check rating value of best match
        // >= 0.7 OK, value is already stored into db, do nothing
        // < 0.7 NO, value isn't stored into db, add it
        if (matchResult.bestMatch.rating < 0.7) {
            //query db to add new prof
            var res = await db.query('INSERT INTO Professor (name) VALUE (?)', [item.prof]);    // jshint ignore: line
            return { data: item.prof, mysql: { insertId: res.insertId } };
        }
        return null;
    }
}

async function updateDBtimes(item) {
    if (item) {
        var query = `INSERT INTO Times (webID, id_course, moduleName, required, id_professor, id_room, date, timestart, timeend, note)  VALUES 
                        (?, (SELECT id FROM Course WHERE Course.csvCode = ?), ?, ?,
                        (SELECT id FROM Professor WHERE Professor.name = ?), (SELECT id FROM Room WHERE Room.name = ?), ?, ?, ?, ?)`;
        var args = [item.webID, item.courseName, item.moduleName, item.required ? 1 : 0, item.prof, item.place.room,
        moment(item.date).format('YYYY-MM-DD'), item.time.start, item.time.end, item.note ? item.note : null];
        var res = await db.query(query, args);
        return { data: item, mysql: { insertId: res.insertId } };
    }
    return null;
}

async function registerLog(results, courseCode) {
    if (results.prof.length || results.times.length)
        await db.query(`INSERT INTO log (action, courseCode, prof, times) VALUES (?, ?, ?, ?)`,
            ['new', courseCode, results.prof.length, results.times.length])
}

async function deleteExtraRecords(mergedData, csvCode) {
    var table = await db.query(`SELECT Times.* 
                                FROM Times JOIN Course ON Times.id_course = Course.id
                                WHERE Course.csvCode = ?`, [csvCode]);

    var dbData = _.filter(table, item => {
        return moment(moment(item.date).format('DD/MM/YYYY') + ' ' + item.timestart, 'DD/MM/YYYY H:mm')
            .isSameOrAfter(today);
    });

    //mergedData    --> dati appena scaricati,                                  priorita' 1
    //dbData        --> dati del database, potrebbero dover essere cambiati,    priorita' 0

    var toDelete = [];
    dbData.forEach(dbItem => {
        var exist = _.find(mergedData, mergedItem => {
            return mergedItem.moduleName === dbItem.moduleName
                && moment(mergedItem.date).format('DD/MM/YYYY') === moment(dbItem.date).format('DD/MM/YYYY')
                && parseInt(mergedItem.time.start.split(':')[0]) === parseInt(dbItem.timestart.split(':')[0])
                && parseInt(mergedItem.time.start.split(':')[1]) === parseInt(dbItem.timestart.split(':')[1])
                && parseInt(mergedItem.time.end.split(':')[0]) === parseInt(dbItem.timeend.split(':')[0])
                && parseInt(mergedItem.time.end.split(':')[1]) === parseInt(dbItem.timeend.split(':')[1]);
        });
        if (!exist) toDelete.push(dbItem);
    });

    toDelete = toDelete.map(item => { return item.id; });
    if (toDelete.length) {
        for (let i = 0; i < toDelete.length; i++)
            await db.query(`DELETE FROM Times WHERE id = ?;`, [toDelete[i]])

        await db.query(`INSERT INTO log (action, courseCode, prof, times) VALUES (?, ?, 0, ?)`, ['delete', csvCode, toDelete.length]);
    }

    return toDelete;
    //var res = await db.query(`DELETE FROM Times WHERE id IN (?);`, [toDelete])
}

/**
 * This function start the scraping process
 * 1- download data from google sheet and from website
 * 2- merge results in a single array
 * 3- filter results by date (keep only new data)
 * 4- insert data into db
 * @param {string} currentCourse the item in the database's table(Course) to be downloaded
 */
async function scrapeOne(currentCourse) {
    debug('[Google Spreadsheet]  dowloading...', true);
    var csvData = await scraperCSV.scrapeByGSheetCode(currentCourse.csvID, currentCourse.csvRange);    //e.g. 1aV....ynY, TSAC-A2!A:J
    debug(csvData.length + ' record found', true);

    debug('\n[ITS Booking Website]  dowloading...', true);
    var webData = await scraperWEB.scrapeByCourseCode(currentCourse.webCode);   //ae.g. TSAC2
    debug(webData.length + ' record found', true);

    debug('\nMerging data...', true);
    var merged = await merge(csvData, webData);
    debug(merged.length + ' record merged', true);

    await db.query('BEGIN;')

    debug('\nChecking for uncompleted records...', true);
    var updated = await uploadUncompletedRecords(merged, currentCourse.csvCode);

    debug('\nFiltering by Date...', true);
    var filtered = await filterByDate(updated);
    debug(filtered.length + ' record filtered', true);

    debug('\nFiltering by already existing records...', true);
    var newData = await filterByExistingRecords(filtered, currentCourse.csvCode);
    debug(newData.length + ' record filtered', true);

    debug('\nUpdating db...', true);
    var results = { prof: [], times: [] };
    for (var i = 0; i < newData.length; i++) {
        var item = newData[i];

        //update professors table
        var resProf = await updateDBprof(item);
        if (resProf) { results.prof.push(resProf); }

        //update times table
        var resTimes = await updateDBtimes(item);
        if (resTimes) { results.times.push(resTimes); }
    }
    debug(results.prof.length + ' profs uploaded', true);
    debug(results.times.length + ' times uploaded', true);

    debug('\nDeteling extra records...', true);
    var deleted = await deleteExtraRecords(merged, currentCourse.csvCode);
    debug(deleted.length + ' record deleted', true);

    await db.query('COMMIT;')

    console.log('\n');

    await registerLog(results, currentCourse.csvCode);
}

async function scrape(index = 0) {
    if (index === courseTable.length) {
        await db.query(`INSERT INTO log (action) VALUES (?)`, ['check']);
        console.log('\n'
            + moment(today).format('YYYY/MM/DD @ H:mm:ss')
            + '  ~  ' + Math.floor((moment() - moment(today)) / 1000) + 's\n\n');
        return;
    }

    try {
        var currentCourse = courseTable[index];
        debug(currentCourse.name + ' - ' + currentCourse.csvCode + ' - ' + currentCourse.type);
        await scrapeOne(currentCourse);
        await scrape(index + 1);
    } catch (err) {
        console.log(err);
        await db.query('ROLLBACK;')
        return;
    }
}

module.exports.main = async function main() {
    //today = moment('29/10/2018 0:00', 'DD/MM/YYYY H:mm');
    today = moment();

    mydb = require(__dirname + '/utils/mydb');
    db = new mydb.Database(cfg.mysql);
    courseTable = await db.query('SELECT * FROM Course WHERE active = 1 ORDER BY id');
    await scrape();
    db.close();
}