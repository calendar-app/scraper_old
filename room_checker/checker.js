"use strict";
const cfg = require(__dirname + '/../config');
var nodemailer = require('nodemailer');
const _ = require('underscore');
const moment = require('moment');
moment.locale('it');

async function check(recipient) {
    var mydb = require(__dirname + '/../utils/mydb');
    var db = new mydb.Database(cfg.mysql);

    var html = `<div style="text-align: center"><img alt="" height="50" width="50" src="cid:cal100" /></div>`;
    var warning = 0;

    var timesTable = await db.query(`SELECT * FROM Times 
                                        WHERE date >= CURDATE() ORDER BY id_course;`);
    var dataOverap = [];
    timesTable.forEach(quizItem => {
        timesTable.forEach(item => {
            if (item.id != quizItem.id
                && moment(item.date).isSame(moment(quizItem.date))
                && moment(item.timestart, 'HH:mm:ss').isSame(moment(quizItem.timestart, 'HH:mm:ss'))
                && moment(item.timeend, 'HH:mm:ss').isSame(moment(quizItem.timeend, 'HH:mm:ss'))
                && parseInt(item.id_professor) == parseInt(quizItem.id_professor)
                && !dataOverap.includes(item.id)
                && !dataOverap.includes(quizItem.id)) {
                dataOverap.push(quizItem.id); dataOverap.push(item.id);
            }
        });
    });

    if (dataOverap.length) {
        var dataOL = await db.query(`
                    SELECT 
                        Times.id         id,
                        Course.name      course,
                        Course.csvCode   code,
                        Times.moduleName module,
                        Professor.name   prof,
                        Times.date       date,
                        Times.timestart  timestart,
                        Times.timeend    timeend
                    FROM Calendar.Course
                        JOIN Calendar.Times on Course.id = Times.id_course
                        JOIN Calendar.Professor on Times.id_professor = Professor.id
                    WHERE Times.id IN (${dataOverap.join(', ')})
                    ORDER BY prof;`);

        if (dataOL.length) {
            warning += dataOL.length;
            console.log(dataOL.length + ' warning!');
            html += `<div style="text-align: center">
                        <h3>Avviso Possibili <b>ORARI SOVRAPPOSTI</b> (${dataOL.length / 2})</h3>
                        Controllare le seguenti coppie di orari:
                    </div><ul>`;

            await dataOL.forEach((item, index) => {
                var hours = { start: item.timestart.split(':'), end: item.timeend.split(':') }
                if (index % 2 == 0 && index > 0) html += `<br/><br/><br/>`
                html +=
                    `<li style="margin-top:20px">
                        <div><b>${item.course}</b></div>
                        <div><b>${item.module}</b></div>
                        <div>Prof: ${item.prof}</div>
                        <div>Data: ${moment(item.date).format('ddd D MMM YY').toLowerCase()}</div>
                        <div>Orario: ${hours.start[0]}${parseInt(hours.start[1]) ? ':' + hours.start[1] : ''} 
                                - ${hours.end[0]}${parseInt(hours.end[1]) ? ':' + hours.end[1] : ''}</div>
                    </li>`;

            });
            html += `</ul><br/><br/>`;
        }
    }

    var dataROOM = await db.query(`
                    SELECT 
                        Times.id         id,
                        Course.name      course,
                        Course.csvCode   code,
                        Times.moduleName module,
                        Professor.name   prof,
                        Times.date       date,
                        Times.timestart  timestart,
                        Times.timeend    timeend
                    FROM Calendar.Course
                        JOIN Calendar.Times on Course.id = Times.id_course
                        JOIN Calendar.Professor on Times.id_professor = Professor.id
                    WHERE Times.id_room IS NULL
                    AND Times.date >= CURDATE()
                    ORDER BY Course.name;`);

    if (dataROOM.length) {
        warning += dataROOM.length;
        console.log(dataROOM.length + ' warning!');
        html += `<div style="text-align: center">
                    <h3>Avviso Dati Mancanti (${dataROOM.length})</h3>
                    Controllare la prenotazione delle aule dei seguenti orari:
                </div><ul>`;

        await dataROOM.forEach(item => {
            var hours = { start: item.timestart.split(':'), end: item.timeend.split(':') }
            html +=
                `<li style="margin-top:20px">
                    <div><b>${item.course}</b></div>
                    <div><b>${item.module}</b></div>
                    <div>Prof: ${item.prof}</div>
                    <div>Data: ${moment(item.date).format('ddd D MMM YY').toLowerCase()}</div>
                    <div>Orario: ${hours.start[0]}${parseInt(hours.start[1]) ? ':' + hours.start[1] : ''} 
                            - ${hours.end[0]}${parseInt(hours.end[1]) ? ':' + hours.end[1] : ''}</div>
                </li>`
        });
        html += `</ul><br/>`;
    }


    if (warning > 0) {
        html += `<em>email generata automaticamente</em><br>Powered by <em>ITS Calendar</em>`;

        var transporter = nodemailer.createTransport({ service: 'gmail', auth: cfg.authmail });

        var mailOptions = {
            to: recipient,
            subject: 'Dati Mancanti - ' + moment().format('DD/MM/YYYY') + ' - ITS Calendar',
            html: html,
            attachments: [{
                filename: 'cal-100.png',
                path: './cal-100.png',
                cid: 'cal100' //same cid value as in the html img src
            }]
        };

        transporter.sendMail(mailOptions, async (e, info) => {
            if (e) console.log(e);
            else console.log('Email sent: ' + info.response + '\n' + info.messageId);

            await db.query(`INSERT INTO log (action, times) VALUES (?, ?)`, [e ? 'mailERROR' : 'mailSEND', warning]);
            db.close();
        });
    } else {
        console.log('Everything OK');
        db.close();
    }
}

module.exports.check = check;