"use strict";
var cron = require('node-cron');
const moment = require('moment');
moment.locale('it');

// At 12:00 on Saturday, Sunday, and Monday
console.log('\nCron started!');
console.log('Execute at 13:00 on Saturday, Sunday, and at 12:00 on Monday');

cron.schedule('0 12 * * SAT,SUN', () => {
    console.log('\n\n' + moment().format('DD/MM/YYYY'));
    require('./checker')
        .check('marco.stevanon@tecnicosuperiorekennedy.it, simonetta.moro@tecnicosuperiorekennedy.it');
});

cron.schedule('0 11 * * MON', () => {
    console.log('\n\n' + moment().format('DD/MM/YYYY'));
    require('./checker')
        .check('marco.stevanon@tecnicosuperiorekennedy.it, simonetta.moro@tecnicosuperiorekennedy.it');
});