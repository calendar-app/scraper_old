"use strict";
const { google } = require('googleapis');
const moment = require('moment');
const googleapi = require('./mygoogleapis');


function scrapeByGSheetCode(code, range) {
    const auth = googleapi.authorize();
    if (auth) { return getTimes(auth, code, range); }
    else { console.log("Unable to authenticate google apis"); }
}

function getTimes(auth, spreadsheetId, range) {
    return new Promise((resolve, reject) => {
        try {
            const sheets = google.sheets({ version: 'v4', auth });
            sheets.spreadsheets.values.get({ spreadsheetId, range }, (err, list) => {
                if (err) { reject(err); return; }
                if (!list) {
                    resolve([])
                    return
                }
                const { data } = list

                var rows = data.values;
                if (rows.length) {
                    var result = [];
                    for (var i = 1; i < rows.length; i++) {

                        var item = rows[i];
                        var course = item[0];
                        var title = item[7];    //remove slash for accent words
                        var required = item[6] ? true : false;
                        var prof = item[8] ? item[8].split('-')[0] : '.';
                        var date = moment(item[3], 'DD/MM/YYYY').toISOString();
                        var start = item[1].split(',');
                        start = start[0] + ':' + (start[1] ? `${start[1]}` : '00');
                        var end = item[2].split(',');
                        end = end[0] + ':' + (end[1] ? `${end[1]}` : '00');
                        var duration = item[5];
                        var note = item[9];

                        result.push({ id: -1, course, title, required, prof, date, time: { start, end, duration }, note });
                    }
                    resolve(result);
                }
                else { reject(new Error('No data found.')); }
            });
        } catch (err) {
            console.log(err); reject(err);
        }
    });
}

module.exports.scrapeByGSheetCode = scrapeByGSheetCode;