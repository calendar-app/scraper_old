"use strict";
const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');

// If modifying these scopes, delete credentials.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];

const TOKEN_PATH = __dirname + '/credentials.json';
const SECRET_PATH = __dirname + '/client_secret.json';

/**
 * Check if secret keys exists and return auth client object
 * to use for api request
 * @return {object} authclient object
 */
function authorize() {
    try {
        // Check if secret keys file exists
        var content = fs.readFileSync(SECRET_PATH);
        const { client_secret, client_id, redirect_uris } = JSON.parse(content).installed;
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

        // Check if token file exist.
        try {
            var token = fs.readFileSync(TOKEN_PATH);
            oAuth2Client.setCredentials(JSON.parse(token));
            return oAuth2Client;
        } catch (err) {
            return console.log("Unable to get token");
        }
    } catch (err) {
        return console.log('Error loading client secret file:', err);
    }
}

/**
 * Get and store new token after prompting for user authorization
 */
function generateNewToken() {
    try {
        var content = fs.readFileSync(SECRET_PATH);
        const { client_secret, client_id, redirect_uris } = JSON.parse(content).installed;
        const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
        const authUrl = oAuth2Client.generateAuthUrl({ access_type: 'offline', scope: SCOPES });
        console.log('Authorize this app by visiting this url:', authUrl);
        const rl = readline.createInterface({ input: process.stdin, output: process.stdout });
        rl.question('Enter the code from that page here: ', code => {
            rl.close();
            oAuth2Client.getToken(code, (err, token) => {
                if (err) { throw err; }
                oAuth2Client.setCredentials(token);

                // Store the token to disk for later program executions
                fs.writeFile(TOKEN_PATH, JSON.stringify(token), err => {
                    if (err) { throw err; }
                    console.log('Token stored to', TOKEN_PATH);
                });
            });
        });
    } catch (err) { console.log("Error while generating new token: ", err); }
}

module.exports.authorize = authorize;
module.exports.generateNewToken = generateNewToken;