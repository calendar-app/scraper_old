"use strict";
const rp = require('request-promise');
const cheerio = require('cheerio');
const moment = require('moment');
moment.locale('it');
const BASE_URL = 'http://www.unipordenone.it/mrbs/search.php';

/**
 * get all course data by course code
 * @param {string} course provide code that identify the course
 * @param {number} index initial offset from witch start parsing
 * @returns {Array} array of scraped object
 */
async function scrapeByCourseCode(course, index = 0) { // jshint ignore:line
	var scrapedData = [];
	while (true) {
		var temp = getPageData(`${BASE_URL}?search=${course}&limit=${index}0`);
		temp = await temp.then(r => { return r }); // jshint ignore:line
		if (temp.length > 0) {
			temp.forEach(item => { scrapedData.push(item); });	// jshint ignore:line
		} else { break; }
		index++;
	}
	return scrapedData;
}

/**
 * fetch data from url and explode data into an array
 * @param {string} uri input url to parse
 * @returns {Promise}
 */
async function getPageData(uri) {   // jshint ignore:line
	const options = { uri, transform: (body) => { return cheerio.load(body); } };
	try {
		const res = await rp(options).then(html => parseHtml(html));    // jshint ignore:line
		return Promise.resolve(res);
	} catch (error) {
		return Promise.reject(error);
	}
}

/**
 * extract data from given html
 * @param {string} html body code of fetched page
 * @returns {Array}
 */
function parseHtml(html) {
	var $ = html;
	var result = [];
	//console.log($('body').html());
	$('table[class=slot] td').each((index, item) => {
		//console.log($(item).html());
		if ($(item).text() !== 'Non ci sono prenotazioni corrispondenti ai criteri di ricerca!') {
			try {
				var id = $(item).find('a').attr('href').split('=')[1];
				var head = $(item).find('a').text();
				head = head.indexOf("-") === -1 ? head.split(' ') : head.split('-');	//prevent error (CORRECT: 'SVILUPPO BACKEND - TSCA1' - WRONG: 'SVILUPPO BACKEND  TSAC1')
				var course = head[0];
				var title = head[1];
				var prof = substr($(item).find('span').text());
				var place = $(item).html();
				place.indexOf("<br>") == -1 ?
					place = place.split('</h6>')[1].split('<p>')[0] :
					place = place.split('<p>')[0].split('<br>')[1];
				var building = place ? place.split(' ')[1] : null;
				var room = place ? place.split('-')[1].trim() : null;
				var date = moment($(item).find('strong').text(), "dddd DD MMMM YYYY").toISOString();
				var time = substr($(item).find('em').text()).split(' - ');

				result.push({ id, course, title, prof, place: { building, room }, date, time: { start: time[0], end: time[1] } });

			} catch (err) {
				//throw new Error(`Unable to parse row\n${$(item).html()}`);
				console.log(err);
			}
		}
	});
	return result;
}

/**
 * remove parentesis from input string
 * @param {string} s string from witch remove parentesis
 * @returns {string}
 */
function substr(s) {
	var start = s.indexOf('(');
	var end = s.indexOf(')');

	//if provided string doesn't contains parentesis return empty
	return start === -1 || end === -1 ? '' : s.substring(start + 1, end);
}

module.exports.scrapeByCourseCode = scrapeByCourseCode;

/* EXAMPLE OF HTML TO PARSE - BEST CASE
   <table class="slot">
        <tbody>
            <tr>
                <td class="data1">
                    <a href="view_entry.php?id=194771">
                        <span class="ev">TSAC1</span> - SVILUPPO BACKEND</a>
                    <h6></h6>
                    <span>(Andrea Dottor)</span>
                    <br>Edificio B - Lab. ITS (L3)
                    <p>
                        <strong>martedì 24 aprile 2018</strong>&nbsp;
                        <em>(14:00 - 18:00)</em>
                    </p>
                </td>
            </tr>
            <tr>...</tr>
        </tbody>
    </table>
*/


/* 
code to solve future bug in parsing function

function parseBuildingRoom(data) {
    if (data.indexOf("<br>") == -1) {
        var br = data.substring(data.indexOf("</h6>") + 5, data.indexOf("<p>")).split('-')
        return [br[0].split(' ')[1].trim(), br[1].trim()];
    } else {
        try {
            var br = data.split('<br>')[1].split('<p>')[0].trim().split('-')
            return [br[0].split(' ')[1].trim(), br[1].trim()]
        } catch (ex) {
            return ["N.A.", "N.A."];
        }
    }
}

function parseDate(data) {
    var date = $(data).find('strong').text().split(' ')
    //var datePurify = date.filter(value => Object.keys(value).length !== 0);
    return moment(date[1] + "-" + date[2] + "-" + date[3], 'DD-MMMM-YYYY', 'it').format('YYYY-MM-DD');
}
*/